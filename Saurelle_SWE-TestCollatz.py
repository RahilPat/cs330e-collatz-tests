#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    #Added cases
        
    def test_read2(self):
        s = "199965 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  199965)
        self.assertEqual(j, 200)
        
    def test_read3(self):
        s = "2000 219860\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2000)
        self.assertEqual(j, 219860)
        
    def test_read4(self):
        s = "50000 187009\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  50000)
        self.assertEqual(j, 187009)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

     # Added cases
     
    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(19999, 9996)
        self.assertEqual(v, 279)

    def test_eval_7(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")


    # Added cases
    
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 984263, 984008, 352)       
        self.assertEqual(w.getvalue(), "984263 984008 352\n")
        
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 281758, 283365, 358)
        self.assertEqual(w.getvalue(), "281758 283365 358\n")
        
    def test_print4(self):
        w = StringIO()
        collatz_print(w, 810273, 809602, 344)
        self.assertEqual(w.getvalue(), "810273 809602 344\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

     # Added cases
     
    def test_solve2(self):
        r = StringIO("931890 931544\n788836 791387\n919454 919499\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "931890 931544 277\n788836 791387 406\n919454 919499 202\n")
       
    def test_solve3(self):
        r = StringIO("655238 655329\n358577 358839\n842747 844169\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "655238 655329 292\n358577 358839 348\n842747 844169 388\n")
        

    def test_solve4(self):
        r = StringIO("869222 869573\n724852 724070 362\n884741 885879\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "869222 869573 388\n724852 724070 362\n884741 885879 370\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
