#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1000)

    def test_read_3(self):
        s = "999900 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999900)
        self.assertEqual(j, 999999)
    
    def test_read_4(self):
        s = "777 778\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  777)
        self.assertEqual(j, 778)

    def test_read_5(self):
        s = "10 0\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 0)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(29000, 28000)
        self.assertEqual(v, 259)

    def test_eval_5(self):
        v = collatz_eval(983000, 984000)
        self.assertEqual(v, 383)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 99999, 351)
        self.assertEqual(w.getvalue(), "1 99999 351\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 994000, 995000, 365)
        self.assertEqual(w.getvalue(), "994000 995000 365\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 14000, 15000, 271)
        self.assertEqual(w.getvalue(), "14000 15000 271\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 1700, 16000, 276)
        self.assertEqual(w.getvalue(), "1700 16000 276\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("1 2\n250 500\n6 10\n27 10098\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 2 2\n250 500 144\n6 10 20\n27 10098 262\n")
        
    def test_solve_3(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve_4(self):
        r = StringIO("21000 20000\n20000 19000\n19000 18000\n18000 17000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "21000 20000 256\n20000 19000 274\n19000 18000 261\n18000 17000 279\n")

    def test_solve_5(self):
        r = StringIO("1700 16000\n16000 15000\n15000 14000\n14000 13000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1700 16000 276\n16000 15000 271\n15000 14000 271\n14000 13000 276\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 0.141s

OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 0.141s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          34      0     12      0   100%
TestCollatz.py      91      0      0      0   100%
------------------------------------------------------------
TOTAL              125      0     12      0   100%
"""
